import { State } from ".";

export default {
  setLoggedIn(state: State, payload: boolean) { 
    state.isLoggedIn = payload 
  },
  setEmail(state: State, payload: string) { 
    state.email = payload 
  },
  setRememberMe(state: State, payload: boolean) { 
    state.rememberMe = payload 
  }
}