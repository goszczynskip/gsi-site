import firebase from '@firebase/app'
import '@firebase/auth'
import { ActionContext, Store } from 'vuex'

import router from '@/router'
import { IState as RootState } from '@/store/state'
import { State } from '.'

export interface ICredentials {
  username: string
  password: string
}

export default {
  logIn({ commit, getters }: ActionContext<State, RootState>, { username, password }: ICredentials) {
    const persistence = getters.rememberMe
      ? firebase.auth!.Auth.Persistence.LOCAL
      : firebase.auth!.Auth.Persistence.SESSION

    firebase.auth!().setPersistence(persistence)
      .then(() => {
        return firebase.auth!().signInWithEmailAndPassword(username, password)
      })
      .then(userCredential => {
        commit('setLoggedIn', true)
        commit('setEmail', userCredential.email)
        router.push('/admin')
      })
      .catch(error => {
        console.log(error)
      })
  },
  logOut({ commit }: ActionContext<State, RootState>) {
    firebase.auth!().signOut().then(() => {
      commit('setLoggedIn', false)
      commit('setEmail', '')
    })
  },
  setRememberMe({ commit }: ActionContext<State, RootState>, newValue: boolean) {
    commit('setRememberMe', newValue)
  },
}
