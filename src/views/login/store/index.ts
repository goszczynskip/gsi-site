import {
  Getter as RootGetter,
  Action as RootAction,
  namespace
} from 'vuex-class'
import store from '@/store'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

export class State {
  isLoggedIn: boolean = false
  email: string =  ''
  rememberMe: boolean = false
}

store.registerModule('login', {
  namespaced: true,
  state: new State(),
  getters,
  actions,
  mutations
})

if (module.hot) {
  module.hot.accept([
    './getters',
    './actions',
    './mutations'
  ], () => {
    store.hotUpdate({
      modules: {
        Login: {
          getters: require('./getters'),
          actions: require('./actions'),
          mutations: require('./mutations')
        }
      }
    })
  })
}

export const Getter = namespace('login', RootGetter)
export const Action = namespace('login', RootAction)
export default store
