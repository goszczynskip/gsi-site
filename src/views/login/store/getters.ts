import { State } from '.'

export default {
  isLoggedIn(state: State) {
    return state.isLoggedIn
  },
  email(state: State) {
    return state.email
  },
  rememberMe(state: State) {
    return state.rememberMe
  }
}
