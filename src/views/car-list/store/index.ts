import { Module } from 'vuex'
import store from '@/store'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import { State } from './state'
import {
  Getter as RootGetter,
  Action as RootAction,
  namespace
} from 'vuex-class'

store.registerModule('CarList', <Module<State, {}>>{
  namespaced: true,
  state: new State(),
  getters,
  actions,
  mutations
})

if (module.hot) {
  module.hot.accept([
    './getters',
    './actions',
    './mutations'
  ], () => {
    store.hotUpdate({
      getters: require('./getters'),
      actions: require('./actions'),
      mutations: require('./mutations')
    })
  })
}

export const Getter = namespace('CarList', RootGetter)
export const Action = namespace('CarList', RootAction)
export default store