import { Reference } from '@firebase/database-types'
import '@firebase/storage'
import { UploadMetadata } from '@firebase/storage-types'

import firebase from '@/firebase'
import { ICarDbModel, ICarModel } from '@/store/state'
import { guid } from '@/utils/guid'
import { IEditedCarModel } from './state'

export interface ICarSaveModel {
  oldModel?: ICarDbModel,
  newModel: ICarModel
}

export function createCar(carModel: IEditedCarModel): Promise<ICarSaveModel> {
    const carInfoRef = createNewCarInfoRef()
    return uploadCarImages(carModel)
      .then(images => createCarDbModelWithImages(carModel, images))
      .then(dbModel => carInfoRef.set(dbModel))
      .then(() => ({
        newModel: Object.assign({}, carModel, { id: carInfoRef.key }),
        oldModel: undefined,
      }))
}

export function deleteCar(key: string): Promise<ICarDbModel> {
  const carInfoRef = getCarInfoRefByKey(key)
  return carInfoRef.once('value')
    .then(snapshot => (snapshot.val() as ICarDbModel))
    .then(model => {
      return deleteCarImages(model)
    })
    .then(() => carInfoRef.remove())
}

export function updateCar(carModel: IEditedCarModel): Promise<ICarSaveModel> {
  const carInfoRef = getCarInfoRefByModel(carModel)
  return carInfoRef
      .once('value')
      .then(snapshot => (snapshot.val() as ICarDbModel))
      .then(oldModel => {
        // TODO: add images synchronization
        return uploadCarImages(carModel)
          .then(images => createCarDbModelWithImages(carModel, images))
          .then(carInfoRef.update)
          .then(() => ({oldModel, newModel: carModel}))
      })
}

function getCarInfoRefByKey(key: string): Reference {
  const database = firebase.database!()
  return database.ref('carsInfo').child(key)
}

function getCarInfoRefByModel(carModel: ICarModel): Reference {
  const database = firebase.database!()
  const carInfoRef = database.ref('carsInfo')
  return carInfoRef.child(carModel['.key'])
}

function createNewCarInfoRef(): Reference {
  const database = firebase.database!()
  const carInfoRef = database.ref('carsInfo')
  return carInfoRef.push()
}

function deleteCarImages(carModel: ICarDbModel): Promise<void[]> {
  const urlsToDelete = []

  if (carModel.mainImage) {
    urlsToDelete.push(carModel.mainImage)
  }

  if (carModel.images) {
    urlsToDelete.push(...carModel.images)
  }

  return deleteStorageObjectsByUrl(urlsToDelete)
}

function deleteStorageObjectsByUrl(urls: string[]): Promise<void[]> {
  const storage = firebase.storage!()
  const deleteCommands = urls.map(url => storage.refFromURL(url).delete() as Promise<void>)
  return Promise.all(deleteCommands)
}

function uploadCarImages(carModel: IEditedCarModel): Promise<ICarImagesDbModel> {
  const uploadCommands = []
  const storage = firebase.storage!()

  let mainImage: ImageDbModel | undefined
  if (carModel.mainImage) {
    const image = createImageDbModel(carModel.mainImage)
    mainImage = image

    const command = storage.ref(image.uniqueName).put(image.blob, image.metadata)
    uploadCommands.push(command)
  }

  let imagesDbModels: ImageDbModel[] | undefined

  if (carModel.images) {
    imagesDbModels = []

    for (const image of carModel.images) {
      const imageDbModel = createImageDbModel(image)
      imagesDbModels.push(imageDbModel)
      uploadCommands.push(
        storage
          .ref(imageDbModel.uniqueName)
          .put(imageDbModel.blob, imageDbModel.metadata),
      )
    }
  }

  return Promise.all(uploadCommands)
    .then(() => ({
      images: imagesDbModels,
      mainImage,
    }))
}

interface ImageDbModel {
  uniqueName: string,
  blob: File
  metadata: UploadMetadata
}

interface ICarImagesDbModel {
  mainImage?: ImageDbModel
  images?: ImageDbModel[]
}

interface ICarDbModelWithImages {
  dbModel: ICarDbModel,
  images: ICarImagesDbModel
}

function createCarDbModelWithImages(car: ICarModel, carImagesModel: ICarImagesDbModel): Promise<ICarDbModel> {
  const dbModel: ICarDbModel = {
    brand: car.brand,
    description: car.description,
    features: car.features,
    fuelType: car.fuelType,
    milage: car.milage,
    model: car.model,
    price: car.price,
    productionDate: car.productionDate,
  }

  let mainImageUrlPromise: Promise<any> = Promise.resolve()

  if (carImagesModel.mainImage) {
    mainImageUrlPromise = getDownloadUrl(carImagesModel.mainImage)
  }

  let imagesUrlPromises: Array<Promise<any>> = []

  if (carImagesModel.images) {
    imagesUrlPromises = carImagesModel.images.map(image => getDownloadUrl(image))
  }

  const allImageUrlsPromise = Promise.all(imagesUrlPromises)
  return Promise.all([mainImageUrlPromise, allImageUrlsPromise])
    .then(([mainImageUrl, imagesUrl]) => {
      if (mainImageUrl) {
        dbModel.mainImage = mainImageUrl
      }

      if (imagesUrl && imagesUrl.length > 0) {
        dbModel.images = imagesUrl
      }

      return clearDbModel(dbModel)
    })
}

function createCarImagesDbModel(car: ICarModel): ICarImagesDbModel {
  const imagesDbModels = []
  if (car.images) {
    for (const image of car.images) {
      imagesDbModels.push(createImageDbModel(image))
    }
  }

  return {
    images: car.images ? imagesDbModels : undefined,
    mainImage: car.mainImage ? createImageDbModel(car.mainImage) : undefined,
  }
}

function createImageDbModel(file: File): ImageDbModel {
  return {
    blob: file,
    metadata: {
      contentType: file.type,
    },
    uniqueName: guid(),
  }
}

function getDownloadUrl(image: ImageDbModel): Promise<string> {
  return firebase.storage!().ref(image.uniqueName).getDownloadURL()
}

function clearDbModel(model: any) {
  return Object.keys(model).reduce((newModel: any, key) => {
    if (model[key] === undefined ||
      model[key] === null ||
      (model[key].length !== undefined && model[key].length === 0)) {
      return newModel
    }

    newModel[key] = model[key]
    return newModel
  }, {})
}
