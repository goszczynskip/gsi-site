import { ICarDbModel } from '@/store/state'

export interface IEditedCarModel {
  ['.key']: string
  brand?: string
  model?: string
  productionDate?: string
  milage?: number
  images: File[]
  fuelType?: string
  price?: number
  features?: string[]
  description?: string
  mainImage?: File
}

export const initialEditedCarModel = {
  ['.key']: '',
  brand: '',
  description: '',
  features: [],
  fuelType: '',
  images: [],
  mainImage: undefined,
  milage: 0,
  model: '',
  price: 0,
  productionDate: '',
}

export const initialCarDbModel = {
  ['.key']: '',
  brand: '',
  description: '',
  features: [],
  fuelType: '',
  images: [],
  mainImage: undefined,
  milage: 0,
  model: '',
  price: 0,
  productionDate: '',
}

export class State {
  public editedCar: IEditedCarModel = initialEditedCarModel
  public carInfo: ICarDbModel = initialCarDbModel
}
