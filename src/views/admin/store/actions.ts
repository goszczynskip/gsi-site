import router from '@/router'
import { IState as RootState } from '@/store/state'
import firebase from '@firebase/app'
import { ActionContext, ActionTree, Store } from 'vuex'
import { firebaseAction } from 'vuexfire'
import { createCar, deleteCar, updateCar } from './firebaseCarActions'
import { editedCarAddImage, editedCarAddMainImage, editedCarRemoveImage, editedCarUpdateImage, editedCarDispose } from './mutationTypes'
import { IEditedCarModel, State } from './state'

export interface IUpdateImageActionPayload {
  newImage: File
  indexToUpdate: number
}

const actions: ActionTree<State, RootState> = {
  deleteCar(context, key: string) {
    return deleteCar(key)
  },

  saveCar({ dispatch, getters }) {
    const carModel = getters.editedCar as IEditedCarModel
    const carExists = !!carModel['.key']

    const saveCar = carExists
      ? updateCar
      : createCar

    return saveCar(carModel)
      .then(({ oldModel, newModel }: any) => {
        return newModel.id
      })
  },

  editedCarAddMainImage({ commit }, file: File) {
    commit(editedCarAddMainImage, file)
  },

  editedCarAddImage({ commit }, file: File) {
    commit(editedCarAddImage, file)
  },

  editedCarRemoveImage({ commit }, index: number) {
    commit(editedCarRemoveImage, index)
  },

  editedCarUpdateImage({ commit }, payload) {
    commit(editedCarUpdateImage, payload)
  },

  disposeEditedCarModel({ commit }) {
    commit(editedCarDispose)
  },

  bindCarInfo: firebaseAction<State>(({ bindFirebaseRef }: any, id) => {
    if (id) {
      const ref = firebase.database!().ref('carsInfo').child(id)
      bindFirebaseRef('carInfo', ref)
    }
  }),

  unbindCarInfo: firebaseAction<State>(({ unbindFirebaseRef }: any) => {
    unbindFirebaseRef('carInfo')
  }),
}

export default actions
