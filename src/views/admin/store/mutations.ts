import Vue from 'vue'
import { MutationTree } from 'vuex'
import {
  editedCarAddImage,
  editedCarAddMainImage,
  editedCarDispose,
  editedCarRemoveImage,
  editedCarUpdateImage,
} from './mutationTypes'
import { initialEditedCarModel, State } from './state'

export interface IUpdateImageMutationPayload {
  newImage: File
  indexToUpdate: number
}

const mutations: MutationTree<State> = {
  [editedCarAddMainImage](state, newImage: File) {
    Vue.set(state.editedCar, 'mainImage', newImage)
  },

  [editedCarAddImage](state, newImage: File) {
    state.editedCar.images.push(newImage)
  },

  [editedCarRemoveImage](state, indexToRemove: number) {
    state.editedCar.images.splice(indexToRemove, 1)
  },

  [editedCarUpdateImage](state, { newImage, indexToUpdate }: IUpdateImageMutationPayload) {
    Vue.set(state.editedCar.images, indexToUpdate, newImage)
  },

  [editedCarDispose](state) {
    state.editedCar = initialEditedCarModel
  },
}
export default mutations
