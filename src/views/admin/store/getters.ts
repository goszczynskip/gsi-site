import { getDefaultCarModel, ICarDbModel } from '@/store/state'
import DataMismatchError from '@/utils/errors/DataMismatchError'
import InvalidOperationError from '@/utils/errors/InvalidOperationError'
import { IEditedCarModel, State } from './state'

export default {
  carInfo(state: State): ICarDbModel {
    return state.carInfo
  },

  editedCar(state: State): IEditedCarModel {
    return state.editedCar
  },

  carImagesAmount(state: State): number {
    const images = state.editedCar.images
    if (images) {
      return images.length
    }

    return 0
  },

  editedCarMainImageName(state: State): string | undefined {
    const editedCarMainImage = state.editedCar.mainImage
    if (editedCarMainImage) {
      return editedCarMainImage.name
    }

    const mainPhotoPath = state.carInfo.mainImage
    if (!mainPhotoPath) {
        return undefined
    }

    const fileName = /\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/.exec(mainPhotoPath)
    if (fileName === null) {
        return undefined
    }

    return fileName[0]
  },

  editedCarImageName(state: State): (id: number) => string {
    return (index: number): string => {
      const photo = state.editedCar.images[index]

      if (photo) {
        return photo.name
      }

      const uploadedPhotos = state.carInfo.images
      if (uploadedPhotos) {
        const uploadedPhoto = uploadedPhotos[index]

        const fileName = /\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/.exec(uploadedPhoto)
        if (fileName === null) {
            throw new DataMismatchError('File url doesn\'t contain file name')
        }

        return fileName[0]
      }

      return ''
    }
  },
}
