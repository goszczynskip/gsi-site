import store from '@/store'
import { Action as RootAction, Getter as RootGetter, namespace } from 'vuex-class'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import { State } from './state'

store.registerModule('Admin', {
  actions,
  getters,
  mutations,
  namespaced: true,
  state: new State(),
})

if (module.hot) {
  module.hot.accept([
    './getters',
    './actions',
    './mutations',
  ], () => {
    store.hotUpdate({
      modules: {
        Admin: {
          actions: require('./actions'),
          getters: require('./getters'),
          mutations: require('./mutations'),
        },
      },
    })
  })
}

export const Getter = namespace('Admin', RootGetter)
export const Action = namespace('Admin', RootAction)
export default store
