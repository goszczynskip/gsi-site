import firebase from '@firebase/app'

const config = {
  apiKey: 'AIzaSyB3LPLpRJEB8XYbltOebqD_LiWMWEhNGSM',
  authDomain: 'gsicars-fc085.firebaseapp.com',
  databaseURL: 'https://gsicars-fc085.firebaseio.com',
  projectId: 'gsicars-fc085',
  storageBucket: 'gsicars-fc085.appspot.com',
  messagingSenderId: '442577948920'
}
firebase.initializeApp(config)

export default firebase

export const isAdmin = () => {
  return import('@firebase/auth').then(() => {
    return new Promise(resolve => {
      if(firebase.auth) {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
          unsubscribe()
          resolve(!(user === null || user.isAnonymous))
        })
      }
    })
  })
}
