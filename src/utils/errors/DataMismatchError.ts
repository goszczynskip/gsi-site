export default class DataMismatchError extends Error {
  constructor(message: string | undefined) {
    super(message)
  }
}
