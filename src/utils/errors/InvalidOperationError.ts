export default class InvalidOperationError extends Error {
  constructor(message: string | undefined) {
    super(message)
  }
}
