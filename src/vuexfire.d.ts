declare module 'vuexfire' {
  import { ActionContext } from 'vuex';
  import { State as RootState } from '@/store'
  interface FirebaseMutation {
    (_: any, context: ActionContext<{}, {}>): void
  }

  interface FirebaseMutations {
    ['vuexfire/OBJECT_VALUE']: FirebaseMutation
    ['VUEXFIRE_ARRAY_INITIALIZE']: FirebaseMutation
    ['vuexfire/ARRAY_ADD']: FirebaseMutation
    ['vuexfire/ARRAY_CHANGE']: FirebaseMutation
    ['vuexfire/ARRAY_MOVE']: FirebaseMutation
    ['vuexfire/ARRAY_REMOVE']: FirebaseMutation
  }
  export const firebaseMutations : FirebaseMutations
  export function firebaseAction<State>(func: (context: ActionContext<State, RootState>, payload: any) => any): any
}
