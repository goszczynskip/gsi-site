import { FirebaseNamespace } from '@firebase/app-types';
import Vue from 'vue'

declare module 'vue/types/options' {

  interface ComponentOptions<V extends Vue> {
    firebase?: FirebaseNamespace;
  }
}