import { firebaseMutations } from 'vuexfire'

export default {
  ...firebaseMutations
}
