import firebase from '@firebase/app'
import '@firebase/database'
import { DataSnapshot } from '@firebase/database-types'
import { ActionContext, ActionTree, Store } from 'vuex'
import { firebaseAction } from 'vuexfire'

import { IState } from './state'

export default {
  bindCarsPage: firebaseAction<IState>(({ bindFirebaseRef }: any) => {
    const ref = firebase.database!().ref('carsInfo').orderByKey().limitToFirst(20)
    bindFirebaseRef('carsPage', ref)
  }),
  unbindCarsPage: firebaseAction<IState>(({unbindFirebaseRef}: any) => {
    unbindFirebaseRef('carsPage')
  }),
}
