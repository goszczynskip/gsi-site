import { IState } from './state'

export default {
  carsPage: (state: IState) => {
    return state.carsPage
  },
}
