export interface ICarModel {
  ['.key']: string
  brand?: string
  model?: string
  productionDate?: string
  milage?: number
  fuelType?: string
  price?: number
  features?: string[]
  description?: string
  mainImage?: File
  images: File[]
}

export interface ICarDbModel {
  ['.key']?: string
  brand?: string
  model?: string
  productionDate?: string
  milage?: number
  fuelType?: string
  price?: number
  features?: string[]
  description?: string
  mainImage?: string
  images?: string[]
}

export const getEmptyCarModel: () => ICarModel
= () => ({
  ['.key']: '',
  features: [],
  images: [],
})

export const getDefaultCarModel: () => ICarModel
= () => ({
  ['.key']: '',
  brand: '',
  description: '',
  features: [],
  fuelType: '',
  images: [],
  mainImage: undefined,
  milage: 0,
  model: '',
  price: 0,
  productionDate: '',
})

export interface IPage<TPage> {
  items: TPage[]
}

export interface IState {
  carsPage: Array<IPage<ICarModel>>
}

export default {
  carsPage: [],
} as IState
