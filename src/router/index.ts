import Vue from 'vue'
import Router from 'vue-router'
import { isAdmin } from '@/firebase'

// const Poznan = () => import('@/views/poznan')
// const Opole = () => import('@/views/opole')
const AdminView = () => import(/* webpackChunkName: "admin" */ '@/views/admin/Index.vue')
const EditItemView = () => import(/* webpackChunkName: "admin" */ '@/views/admin/EditItem.vue')
const AdminListView = () => import(/* webpackChunkName: "admin" */ '@/views/admin/AdminList.vue')

const CarListView = () => import('@/views/car-list/Index.vue')
const LoginView = () => import('@/views/login/Index.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'CarList',
      component: CarListView
    },
    {
      path: '/admin',
      component: AdminView,
      beforeEnter: (to, from, next) => {
        isAdmin()
          .then((isAdminValue) => {
            if (isAdminValue) {
              next()
            } else {
              next('/login')
            }
          })
      },
      children: [
        { path: '', redirect: 'list' },
        { name: 'Admin-List', path: 'list', component: AdminListView },
        { name: 'Item', path: 'list/:id', component: EditItemView, props: true },
        { name: 'Add', path: 'add', component: EditItemView }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginView,
      beforeEnter: (to, from, next) => {
        isAdmin()
          .then((isAdminValue) => {
            if (isAdminValue) {
              next('/admin')
            } else {
              next()
            }
          })
      }
    }
  ]
})
